.. _parametrage:

###########
Paramétrage
###########

Tous les paramétrages se font par l’intermédiaire de formulaires.
Toutes la partie paramétrage est accessible en cliquant sur *Administration & Paramétrage* (dans la bandeau de l'application).
Ensuite, 

.. figure:: menu_parametrage.png

Dans l’optique openMairie, l’application présente un tableau permettant de choisir l’enregistrement à modifier ou à supprimer, ou de créer un nouvel enregistrement.
Les éléments paramétrables sont :


- la collectivité : le nom de la collectivité.

- les utilisateurs : login, mot de passe et profil des utilisateurs.

- les profils : les différents profils de sécurité.

- les droits : l'association entre un objet et son profil d'accés

- les paramètres généraux : type d'unité pour les imports, nombre d'unités affichées par page, ...

- le tableau de bord : widget du tableau de bord

- les unités : les unités et leurs liens

- les types d'unités : liste des types d'unités

- les candidats : liste des candidats

- les partis politiques : liste des partis politique

- les types d'élection : liste des types d'élection

- les tranches horaires : liste des tranches horaires

- les acteurs : liste des acteurs disponibles pour la configuration de délégation

- les modèles de portail web : liste des modèles d'affichage du portail web

- les plans : liste des plans

- les animations : liste des modèles d'affichage pour l'animation

- le découpage administratif : liste des cantons, circonscriptions, communes et départements

L’utilisateur doit faire attention au fait que s’il supprime un enregistrement utilisé dans d’autres tables, des données seront inutilisables.

.. toctree::

    parametrage_general.rst
    type_election.rst
    unite.rst
    candidat.rst
    tranche.rst
    animation.rst
    parametrage_web.rst
    plan.rst
    decoupage_administratif.rst
 
