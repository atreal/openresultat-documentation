.. _parametrage_general:

Collectivité
============

L'accès au paramétrage de la collectivités se fait par le menu **Administration & Paramétrage**, dans la rubrique *collectivité*. 
En cliquant sur le lien de la collectivité, puis sur modifier, vous allez accéder au formulaire de modification.

.. figure:: form_collectivite.png

   Formulaire de la collectivité

.. _administration_parametre:

Paramètres généraux
===================

Les paramètres généraux sont accessible depuis le menu **Administration -> Paramètre**

Paramètres métier :

- ville : nom qui apparait dans le menu haut et dans les éditions.

- id_default_heure_ouverture : tranche (clé primaire) de l'heure d'ouverture par défaut d'une élection. Si le paramètre n'est pas renseigné aucune valeur par défaut n'est positionnée.

- id_default_heure_fermeture : tranche (clé primaire) de l'heure de fermeture par défaut d'une élection. Si le paramètre n'est pas renseigné aucune valeur par défaut n'est positionnée.

- nb_sieges_default : nombre de sièges au conseil municipal par défaut d'une élection. Si le paramètre n'est pas renseigné aucune valeur par défaut n'est positionnée.

- nb_sieges_com_default : nombre de sièges au conseil communautaire par défaut d'une élection. Si le paramètre n'est pas renseigné aucune valeur par défaut n'est positionnée.

- nb_sieges_mep_default : nombre de sièges au conseil métropolitain par défaut d'une élection. Si le paramètre n'est pas renseigné aucune valeur par défaut n'est positionnée.

- nombre_unite : nombre d'unités à afficher dans le formulaire des unités de l'élection.

- id_type_unite_bureau_de_vote : identifiant du type d'unités récupérées lors de l'imports des bureaux de vote.

Paramètrages des éditions
=========================

Les éditions sont gérées à l'aide d'état et de sous-états, accessible depuis l'onglet **Paramétrage** du menu.

Toutes les éditions de la v1 ont été reprises dans la v2. Les tableaux de ces éditions (hors proclamations) sont réalisés à l'aide de champ de fusion liés à l'élection. Ces champs de fusion sont les suivants :

    - [election.etat_resultat] : Etat resultat par bureau avec totaux (avec colonne *id canton*)
    - [election.etat_participation] : 	Etat de la participation par bureau par tranche horaire avec totaux et pourcentage (prévu pour le format A3 paysage, largeur fixée à 400mm)
    - [election.etat_resultat_perimetre_opt1]	Etat resultat par bureau regroupe par canton avec totaux et pourcentage (avec colonne *vote sur emargement*)
    - [election.etat_resultat_perimetre_opt2]	Etat resultat par bureau regroupe par canton avec totaux (avec colonne *vote sur emargement* et *procuration*)
    - [election.etat_resultat_perimetre_opt3]	Etat resultat par bureau regroupe par canton avec totaux et pourcentage (avec colonne *vote sur emargement* et *procuration*)
    - [election.etat_resultat_globaux_opt1]	Etat resultat par bureau avec total en pourcentage (avec colonne *vote sur emargement* et *procuration*)
    - [election.etat_resultat_globaux_opt2]	Etat resultat par bureau avec total en pourcentage
    - [election.etat_resultat_prefecture]	Etat resultat par bureau (sans le *nom de bureau*) avec totaux (avec colonne *vote sur emargement*)
    - [election.etat_proclamation]	Tableau de proclamation des résultats définitifs
    - [election.etat_proclamation_sieges_cm_cc]	Tableau de proclamation des résultats définitifs avec répartition des sièges
    - [election.etat_proclamation_sieges_cmp]	Tableau de proclamation des résultats définitifs avec répartition des sièges métropolitains

Tableau de bord
===============

Le paramétrage se fait depuis le menu **Administration & Paramétrage**, dans la rubrique *composition*. 

Les widgets disponibles sont présentés dans la partie ergonomie (voir :ref:`Widgets '<widgets>`)

.. figure:: tableau_de_bord.png

   Tableau de bord
