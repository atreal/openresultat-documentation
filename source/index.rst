.. openresultat documentation master file.

==============================
openRésultat 2.2 documentation
==============================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.

openRésultat est un logiciel libre qui permet la gestion des résultats électoraux et l'animation des soirées électorales par l'affichage des résultats. Ce logiciel effectue le traitement des résultats électoraux pour tout type d’élections. L’application contient plusieurs outils comme la création de l’élection, le paramétrage de l’élection, la saisie de la participation, la saisie des résultats (première centaine et élection), la création des états, l’analyse des résultats, l’animation des affichages, le transfert en Préfecture, l’archivage des résultats et bien plus encore. http://www.openmairie.org/catalogue/openresultat

Ce document a pour but de guider les utilisateurs et les développeurs dans la prise en main du projet. Bonne lecture et n’hésitez pas à venir discuter du projet avec la communauté à l’adresse suivante : https://communaute.openmairie.org/c/openresultat


Historique de versions
======================

.. toctree::

   history/index.rst


Manuel de l'utilisateur
=======================

.. toctree::

   manuel_utilisateur/index.rst


Guide du développeur
====================

.. toctree::

   guide_developpeur/index.rst


Contributeurs
=============

(par ordre alphabétique)

* Carole Garcin
* Sophie Lacroix
* Jean-Yves Madier de Champvermeil
* Florent Michon
* François Raynaud
* Sofien Timezouaght
