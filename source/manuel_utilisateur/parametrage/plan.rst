.. plan.rst

Sur le portail web, il est possible d'afficher les bureaux de votes sur des plans. Ces bureaux de votes sont affichés différemment si leurs résultats ont été envoyés à l'affichage. Il est également possible de consulter les résultats d'un bureau en cliquant dessus, sur le plan.

Pour pouvoir utiliser cet affichage, il faut d'abord paramétrer les plans. Ce paramétrage se fait en deux parties : le paramétrage du plan et le placement des unités sur ce plan.

Paramétrage des plans
=====================

Le paramétrage des plans se fait depuis la rubrique *plan*.

.. figure:: tab_plan.png

   Listing des plans

Le formulaire est composé de six champs :

- libellé : nom du plan qui sera notamment affiché sur le portail web
- image plan : champ permettant de sélectionner le plan à afficher
- image unités arrivées : choix de l'icône représentant les unités dont les résultats ont été transmis au portail web
- image unités non arrivées : choix de l'icône réprésentant les unités dont les résultats n'ont pas été transmis au portail web
- par défaut : case à cocher permettant de définir si les plans doivent être associés par défaut aux élection. Si cette case est cochée, lors de la création de l'élection, le plan sera automatiquement associé à l'élection.
- largeur des icônes : largeur des icônes en pixel

.. figure:: form_plan.png

   Formulaire d'un plan

Positionnement des unités
=========================

Le placement des unités sur le plan se fait en choisissant un plan et en cliquant sur l'onglet *plan unite*.

.. figure:: tab_plan_unite.png

   Listing des unités placées sur le plan

Le formulaire de paramétrage de l'affichage des unités sur le plan est composé des champs suivant :
    
- unité : choix de l'unité à placer sur le plan.
- unités arrivées : icône personnalisée de l'unité lorsque ses résultats sont arrivés. Si une icône est sélectionnée dans ce formulaire c'est cette icône qui sera utilisée plutôt que l'icône par défaut choisie lors du paramétrage du plan.
- unités non arrivées : idem champs précédent mais pour les résultats non arrivés
- [X] et [Y] : position de l'unité sur le plan
- largeur des icônes : largeur et hauteur des icônes en pixel

.. figure:: form_plan_unite.png

   Formulaire d'une unité du plan

En cliquant sur l'icône de prévisualisation (à droite du champ [Y]) une fenêtre s'ouvre avec le plan et l'unité placée dessus aux coordonnées voulues.
À partir de cet écran, il suffit de placer l'unité à l'endroit voulu sur le plan et de double cliquer pour que les coordonnées de l'unité soient enregistrées dans le formulaire.

.. figure:: previsualisation.png

   Fenêtre de prévisualisation
