.. _simulation:

Étape de simulation
===================

L'étape de simulation interviens juste après l'étape de paramétrage.
Cette étape à pour but de permettre à l'utilisateur de réaliser des tests avant de commencer la saisie définitive des résultats de l'élection.

Cette étape est la plus permissive car elle permet de modifier le paramétrage, saisir les résultats ou encore calculer le nombre de sièges en même temps.
L'utilisateur doit donc être rigoureux pour éviter les erreurs lors de ses tests.

.. figure:: portlet_simulation.png

   Actions disponibles

Cependant, le tableau de bord de l'élection est là pour aider l'utilisateur au cours de cette étape.
A l'étape de simulation, ce tableau permet de voir d'un seul coup d'oeil les résultats enregistrés pour chaque unités et de savoir si les résultats ont été publié ou pas.
Pareil avec la participation.

.. figure:: tb_simulation.png

   Tableau de bord de l'étape simulation

.. warning::
   Lors du passage à l'étape de saisie, les résultats et la participation enregistrés durant l'étape de simulation sont réinitialisés.
   Il est cependant, possible d'empêcher cette réinitialisation. Pour cela, il faut cocher la case *garder les résultats après la simulation*.
   
.. warning::
   En cas de retour à l'étape de paramétrage les résultats et la participation sont réinitialisés.
   Il n'est pas possible d'empêcher cette réinitialisation. Il est donc conseillé, si vous souhaitez conserver vos résultats de modifier le paramétrage pendant l'étape de simulation.

.. note::
   Le nombre d'inscrit ne sera pas réinitialisés lors du changement d'étape.
