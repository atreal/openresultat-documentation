.. _saisie:

Étape de saisie
===============

L'étape de saisie est l'étape au cours de laquelle l'utilisateur va venir saisir les résultats, les centaines et la participation.

Durant cette étape, l'utilisateur peut saisir et publier les résultats.
Il peut accéder au portail web et aux animations pour les consulter.
Il peut également à tous moment ajouter, supprimer et/ou modifier les animations.
Il a également accès aux éditions et au csv des résultats (hors csv d'envoi à la préfecture et édition de la proclamation des résultats).

.. figure:: portlet_saisie.png

   Actions disponibles

Saisie des centaines
--------------------

La saisie des centaines se fait depuis l'onglet *unité(s)*. Il faut ensuite sélectionner l'unité dont on souhaite saisir la centaine.

Dans le menu de l'unité, il faut cliquer sur l'action qui porte le nom de la centaine (*saisir résultat : nom centaine*) pour accéder au formulaire de saisie de la centaine.
Pour finir saisir les résultats et valider le formulaire pour que la saisie soit enregistrée.

.. warning::
   Le nombre d'inscrit n'est pas modifiable. Il est récupéré à partir du nombre d'inscrit de l'élection pour l'unité correspondante.
   Il faut donc commencer par saisir/importer les inscrits pour l'élection et ensuite saisir les centaines.

.. figure:: form_saisie_centaine.png

   Formulaire de saisie des résultats pour une centaine

Pour faciliter la consultation des résultats, un tableau contenant la liste des unités et leurs résultats est affiché en consultation de la centaine.

.. figure:: tab_centaine.png

   Tableau de bord de la centaine

Saisie des résultats
--------------------

La saisie des résultats se fait depuis l'onglet *unité(s)*.

Dans un premier temps, choisir l'unité à saisir puis cliquer sur l'action *Saisir les résultats* pour accéder au formulaire de saisie.

.. note::

   Les résultats des périmètres ne peuvent pas être saisis. Cependant, ils sont automatiquement mis à jour lorsque les résultats des unités qu'ils contiennent sont saisis.

.. figure:: form_saisie_resultats.png

   Formulaire de saisie des résultats

Une fois le formulaire validé, un message préviens l'utilisateur si il y a une erreur de saisie.
La colonne *saisie* du listing des unités et du tableau de bord de l'élection affiche également un warning si la saisie est erronée.

.. warning::

   Si la délégation de saisie est active, seuls les utilisateurs autorisés auront accés au formulaire de saisie.

Une fois la saisie réalisée, il est possible de la faire valider. Pour cela, utiliser l'action *Demande de validation* du portlet de l'unité.
L'unité sera alors en attente de validation et ses résultats ne pourront plus être modifiés.

.. figure:: portlet_unites.png

   Portlet d'action de l'unité

Pour valider les résultats, il faut ensuite cliquer sur l'action *Valider les résultats*.

.. note::

   Si vous avez séléctionné l'option de validation des résultats avant la publication, les actions de publication deviendront accessible à ce moment.

   Si cette option est activée et que l'option de publication l'est aussi, la validation des résultats entrainera leurs publication.

Les résultats validés ou en cours de validation ne pourront plus être saisi. Pour pouvoir modifier les résultats, utiliser l'action *reprendre la saisie*.
L'unité retrouvera son statut *en cours de saisie* et les résultats pourront être modifiés avant de redemander leur validation.

Saisie de la participation
--------------------------

La saisie de la participation se fait depuis l'onglet *participation(s)*.

Si la délégation de saisie de la participation est active la saisie de la participation se fera par unité sinon elle est faite par tranche horaire.

1. Participation par tranche horaire

Dans un premier temps, choisir l'horaire à saisir, puis cliquer sur l'action *Saisir la participation* pour accéder au formulaire de saisie.

.. note::

   La participation des périmètres ne peut pas être saisie. Cependant, elle est mise à jour lorsque la participation des unités de saisie est remplie.

.. figure:: form_saisie_participation.png

   Formulaire de saisie de la participation par tranches horaires

2. Participation par unité

Dans un premier temps, choisir l'unité à saisir, puis cliquer sur l'action *Saisir la participation* pour accéder au formulaire de saisie.

.. note::

   La participation des périmètres ne peut pas être saisie, elle est automatiquement mise à jour au fur et à mesure du remplissage des unités de saisie.

.. figure:: form_saisie_participation_unite.png

   Formulaire de saisie de la participation par unité

.. warning::

   Si la délégation de saisie de la participation est active, seuls les utilisateurs autorisés auront accés au formulaire de saisie.


Publication
-----------
L'affichage des animations et du portail web nécessite de récupérer les résultats et la participation.
Il faut donc publier les résultats et la participation pour pouvoir afficher correctement les données sur le portail web et les animations.

La publication peut se faire de trois manières :

1. Publication automatique :
   Si l'option de publication automatique est activée, les résultats seront automatiquement publiés suite à la validation du formulaire de saisie.

   Si l'option de validation avant publication est activée, les résultats ne seront automatiquement publiés que lors de leur validation.

   Par défaut, les résultats en erreur ne seront pas publier automatiquement à moins que l'options de publication automatique des résultats en erreur ne soit activée.
   
   La participation est automatiquement publiée (que l'option de publication automatique soit active ou pas) à partir du moment ou le formulaire de participation est entièrement rempli.
   C'est à dire à partir du moment où toutes les tranches horaires ou toutes les unités sont saisies dans le formulaire.

2. Publication avec la case d'envoi :
   Il est également possible de publier les résultats en cochant les cases *envoi web* et *envoi aff* du formulaire de saisie.
   Si ces cases sont cochées, lors de la validation du formulaire les résultats saisis seront publiés sur le web et les animations.
   Si ces cases sont décochées, lors de la validation du formulaire les résultats saisis seront dépubliés.

.. note::

   Les boutons de publication ne sont utilisable que pour la publication des résultats, pas pour celle de la participation.
   Les boutons de publication ne sont accessible que si l'option de publication automatique est désactivée.

3. Publication à l'aide d'action :
   La dernière solution consiste à utiliser les actions *envoi à l'animation et envoi au portail web* (résultats) et *affichage tranche et affichage web* (participation).
   En cliquant, sur ces actions les résultats seront envoyés à l'animation et à la page web.
   
   Il est également possible de dépublier les résultats à l'aide des actions *supprimer de l'animation et supprimer du portail web* (résultats) et *annuler résultat* (participation).

.. note::

   Si l'option de validation avant publication est activée, les actions de publication ne seront pas accessible avant la validation.

.. figure:: portlet_unites.png

   Portlet des résultats

.. figure:: portlet_participation.png

   Portlet de la participation

Pour aider l'utilisateur lors de la publication les colonnes aff et web des listings des unités et de la participation indique si les résultats ont été publiés ou pas.

De la même manière, le tableau de bord de l'élection permet de voir quels éléments ont été publiés ou sont en attente de publication.

.. figure:: tb_saisie.png

   Tableau de bord de l'étape saisie
